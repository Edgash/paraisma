package vista;

import dao.DAOFactory;
import dao.GenericDAO;
import modelo.Cliente;

import java.util.List;

public class Prueba {
    static GenericDAO<Cliente> daoBD;
    static GenericDAO<Cliente> daoFILE;

    public static void main(String[] args) throws Exception {
        DAOFactory df = new DAOFactory();

        daoBD = df.getDAO(DAOFactory.JDBC);
        daoFILE = df.getDAO(DAOFactory.FILE);

        List<Cliente> clientesBD = daoBD.findAll();
        List<Cliente> clientesFile = daoFILE.findAll();

        for(Cliente c: clientesFile){
            daoFILE.delete(c);
        }
        for(Cliente c: clientesBD){
            daoFILE.insert(c);
            System.out.println(c);
        }
    }
}
