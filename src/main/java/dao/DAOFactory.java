package dao;

import modelo.Cliente;

public class DAOFactory {
    public final static int JDBC = 1;
    public final static int FILE = 2;

    public GenericDAO<Cliente> getDAO(int tipo) throws Exception{
        GenericDAO<Cliente> dao = null;
        switch (tipo){
            case JDBC:
                dao = new ClienteDAOImpl();
                break;
            case FILE:
                dao = new ClienteDAOFileImpl();
                break;
            default:
                dao = null;
        }
        return dao;
    }
}
